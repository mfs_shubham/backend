<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

define( 'FS_METHOD', 'direct' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'mindfire' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mindfire' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3kT-6dQc<HiM!7=d}Du KELL>CsrG!DX^iyrw%_sZl~S&D*l|PaPF(k$sDWI610A' );
define( 'SECURE_AUTH_KEY',  'Bb+$VI~U-^C,hrF~|+9U%~Na!>FEy;y&Z,GixUe$IlhU+00Ba>;/sjM{L`19~wi<' );
define( 'LOGGED_IN_KEY',    'Q}z%Ig.*x79ex/gMFA,QZEcZ(U]/xg[{i@uuJ<TYPvKIJH`6|~SGnVs 3W43*a?c' );
define( 'NONCE_KEY',        'Fo6rU?yr*=wW>,oKSmEYVem02W[{#t%`i]$y7AYH0 ;qI~oPBiV#;BxwlId=<laW' );
define( 'AUTH_SALT',        'aJ%;/oal;v8oP,L+JzBo76nkuw7qn*|Un,8KIj.pv=vhye*n0or/Huc7zre7DNI:' );
define( 'SECURE_AUTH_SALT', 'E$g>VxCEEH^r7,X1 q6r_;f*kUW -X(1 PoV!HB)iQ>Ci_){8{(m$.j0FdBBwnE{' );
define( 'LOGGED_IN_SALT',   ']$@COU-_L^A} %9-)511,I,~#@_mqO4iD:Y5QCJFRVT4mh*@Ppgo?pLt#M pYm|e' );
define( 'NONCE_SALT',       '.z/VW80VCVY-{:EBw_EBwi>iYFR&V<[ZV)q%5<(-6tqXBmyyp5{-y,]hkdK`_<$7' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
