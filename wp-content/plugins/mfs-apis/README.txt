=== Plugin Name ===
Donate link: https://www.mindfiresolutions.com/
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 5.7
Stable tag: 5.7
License: Mindfire
License URI: https://www.mindfiresolutions.com/

MFS Apis plugin is used to provide REST APIs for schedule events of wordpress users.

== Description ==
* MFS Apis plugin is used to provide REST APIs for schedule events of wordpress users.
* This plugin can be activated after activating 'Apointment Hour Booking' plugin.
