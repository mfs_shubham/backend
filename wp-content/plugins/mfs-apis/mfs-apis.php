<?php
/**
* Plugin Name: MFS APIs
* Description: This plugin is used to create REST APIs end points to manage user appointments as per available time slots for particular dates. To activate this plugin you need to activate 'Appointment Booking Calendar' plugin first.
* Version: 99.5
* Author: Mindfire Solutions
* Author URI: https://www.mindfiresolutions.com/
**/

define( "PARENT_PLUGIN", "appointment-hour-booking/app-booking-plugin.php" );
define( "PARENT_PLUGIN_NAME", "Appointment Hour Booking Plugin" );

define( "MFS_API_PLUGIN", __FILE__ );
define( "MFS_API_PLUGIN_NAME", "MFS APIs Plugin" );

require_once( 'includes/class-mfs-db.php' );
require_once( 'includes/class-mfs-apis.php' );

new APIs( MFS_API_PLUGIN, MFS_API_PLUGIN_NAME, PARENT_PLUGIN, PARENT_PLUGIN_NAME );
