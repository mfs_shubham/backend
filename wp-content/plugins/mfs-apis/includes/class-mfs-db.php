<?php
if( !class_exists( 'DB_Help' ) ) {

  class DB_Help {
    protected $eventTable;
    protected $scheduleTable;

    function __construct( ) {
      $this->eventTable = "cpappbk_forms";
      $this->scheduleTable = "cpappbk_messages";
    }

    /**
    * Name: mfs_get_events_by_userid
    * Author: Shubham
    * Description: Used to fetch events by user ID from the database
    **/
    function mfs_get_events_by_userid( $userId ){
      global $wpdb;
      $rows = $wpdb->get_results("SELECT id, form_name, cp_user_access FROM " . $wpdb->prefix . $this->eventTable . " ORDER BY form_name");
      $forms = array();
      foreach ( $rows as $item ) {
        if ( @in_array( $userId, unserialize( $item->cp_user_access ) ) ) {
          $forms[] = array ( 'value' => $item->id, 'label' => $item->form_name );
        }
      }

      return $forms;
    }

    /**
    * Name: mfs_get_event_data
    * Author: Shubham
    * Description: Used to fetch event details by event ID from the database
    **/
    function mfs_get_event_data( $eventId ){
      global $wpdb;
      $result = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix.$this->eventTable . " WHERE id = " . $eventId );
      if( !empty( $result ) )
      return $result[0];
      else
      return array();
    }

    /**
    * Name: mfs_get_event_booked_slot
    * Author: Shubham
    * Description: Used to fetch event booked slots from the database
    **/
    function mfs_get_event_booked_slot( $eventId ){
      global $wpdb;
      $events = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . $this->scheduleTable . " WHERE formid = '" . intval( $eventId ) . "'" );

      return $events;
    }

    /**
    * Name: mfs_book_slot
    * Author: Shubham
    * Description: Used to insert schedule data in the database
    **/
    function mfs_book_slot( $eventId, $email, $data, $serializedData ){
      global $wpdb;
      $rowsAffected = $wpdb->insert(
        $wpdb->prefix . $this->scheduleTable,
        array(
          'formid'      => $eventId,
          'time'        => current_time('mysql'),
          'ipaddr'      => ( get_option( 'cp_cpappb_storeip', CP_APPBOOK_DEFAULT_track_IP ) ? $_SERVER['REMOTE_ADDR'] : '' ),
          'notifyto'    => $email,
          'posted_data' => $serializedData,
          'data'        => $data,
          'whoadded'    => 0
        )
      );

      return $rowsAffected;
    }
  }
}
