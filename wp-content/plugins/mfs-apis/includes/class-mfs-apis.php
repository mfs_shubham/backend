<?php
if( !class_exists( 'APIs' ) ) {

  class APIs {
    protected $mfsPlugin;
    protected $mfsPluginName;
    protected $parentPlugin;
    protected $parentPluginName;
    protected $dbObject;

    function __construct( $mfsPlugin, $mfsPluginName, $parentPlugin, $parentPluginName ) {
      $this->mfsPlugin = $mfsPlugin;
      $this->mfsPluginname = $mfsPluginName;
      $this->parentPlugin = $parentPlugin;
      $this->parentPluginName = $parentPluginName;
      $this->dbObject = new DB_Help();

      add_action( 'rest_api_init', [$this, 'init'] );
      register_activation_hook( $this->mfsPlugin, [$this, 'mfs_dependency_activation'] );
      register_deactivation_hook( $this->parentPlugin, [$this, 'mfs_dependency_deactivation'] );
    }

    /**
    * Name: init
    * Author: Shubham
    * Description: Registering API routes and calling methonds
    **/
    function init() {
      register_rest_route(
        'apis/v1', '/mfs_login',
        array(
          'methods'  => 'POST',
          'callback' => [$this, 'mfs_login'],
          // 'permission_callback' => 'check_access'
          'permission_callback' => '__return_true'
        )
      );

      register_rest_route(
        'apis/v1', '/mfs_user_events',
        array(
          'methods'  => 'GET',
          'callback' => [$this, 'mfs_get_user_events'],
          'permission_callback' => '__return_true'
        )
      );

      register_rest_route(
        'apis/v1', '/mfs_timeslots',
        array(
          'methods'  => 'GET',
          'callback' => [$this, 'mfs_get_user_timeslots'],
          'permission_callback' => '__return_true'
        )
      );

      register_rest_route(
        'apis/v1', '/mfs_book_slot',
        array(
          'methods'  => 'POST',
          'callback' => [$this, 'mfs_book_event_slot'],
          'permission_callback' => '__return_true'
        )
      );
    }

    /**
    * Name: mfs_dependency_activation
    * Author: Shubham
    * Description: Check plugin dependencies
    **/
    function mfs_dependency_activation() {
      if( !in_array( $this->parentPlugin, apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
        deactivate_plugins( plugin_basename( $this->mfsPlugin ) );
        wp_die( __( 'Please install and activate ' . $this->parentPluginName . ' to activate ' . $this->mfsPluginname . ' !', $this->parentPluginName ), 'Plugin dependency check', array( 'back_link' => true ) );
      }
    }

    /**
    * Name: mfs_dependency_deactivation
    * Author: Shubham
    * Description: Check dependency plugin deactivation
    **/
    function mfs_dependency_deactivation(){
      if( in_array( plugin_basename( $this->mfsPlugin ), apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
        wp_die( __( 'Please deactivate ' . $this->mfsPluginname . ' first !', $this->parentPluginName ), 'Plugin dependency check', array( 'back_link' => true ) );
      }
    }

    /**
    * Name: mfs_success_response
    * Author: Shubham
    * Description: Used to return success response
    **/
    function mfs_success_response( $data, $status ) {
      $response = new WP_REST_Response( $data );

      if( $status && $status != "" )
      $response->set_status( $status );

      return $response;
    }

    /**
    * Name: mfs_error_response
    * Author: Shubham
    * Description: Used to return error response
    **/
    function mfs_error_response( $slug, $message, $status ) {
      return new WP_Error( $slug, __( $message ), array( 'status' => $status ) );
    }

    /**
    * Name: mfs_format_time_slot
    * Author: Shubham
    * Description: Used to return time slot in required format
    **/
    function mfs_format_time_slot( $hours, $minutes ) {
      if( $hours < 12 ) {
        $slot = $hours . ":$minutes AM";
      } else if( $hours == 12 ) {
        $slot = $hours . ":$minutes PM";
      } else{
        $slot = ( $hours - 12 ) . ":$minutes PM";
      }

      return $slot;
    }

    /**
    * Name: mfs_login
    * Author: Shubham
    * Description: Used to validate user login credentials
    * API Route: /apis/v1/mfs_login
    * Method: POST
    * Mandatory Params: username & password
    **/
    function mfs_login( WP_REST_Request $request ) {
      $parameters = $request->get_body_params();

      if( !isset( $parameters["username"] ) || $parameters["username"] == "" ) {
        return $this->mfs_error_response( 'missing_parameters', 'Username is mandatory !', 400 );
      } else if( !isset( $parameters["password"] ) || $parameters["password"] == "" ) {
        return $this->mfs_error_response( 'missing_parameters', 'Password is mandatory !', 400 );
      } else{
        $creds = array( 'user_login' => $parameters["username"], 'user_password' => $parameters["password"], 'remember' => true );
        $user = wp_signon( $creds, false );

        if ( is_wp_error( $user ) ) {
          return $this->mfs_error_response( 'invalid_user', 'Invalid Credentials !', 400 );
        } else{
          $data = array(
            'code' => 'success',
            'message' => "Login successfully",
            'data' => array(
              "userId" => $user->ID,
              "email" => $user->data->user_email,
              "name" => $user->data->display_name,
            )
          );

          return $this->mfs_success_response( $data, 200 );
        }
      }
    }

    /**
    * Name: mfs_get_user_events
    * Author: Shubham
    * Description: Used to return user events
    * API Route: /apis/v1/mfs_user_events
    * Method: GET
    * Mandatory Params: username
    **/
    function mfs_get_user_events( WP_REST_Request $request ) {
      $parameters = $request->get_params();

      if( !isset( $parameters["username"] ) || $parameters["username"] == "" ) {
        return $this->mfs_error_response( 'missing_parameters', 'Username is mandatory !', 400 );
      } else {
        $userData = get_user_by( 'login', $parameters["username"] );
        if( $userData === false ) {
          return $this->mfs_error_response( 'invalid_parameters', 'Invalid username !', 400 );
        } else {
          $result = $this->dbObject->mfs_get_events_by_userid( $userData->data->ID );
          $data = array(
            'code' => 'success',
            'message' => "User events fetched successfully",
            'data' => $result
          );

          return $this->mfs_success_response( $data, 200 );
        }
      }
    }

    /**
    * Name: mfs_get_user_timeslots
    * Author: Shubham
    * Description: Used to return user available time slos as per event_id & date
    * API Route: /apis/v1/mfs_timeslots
    * Method: GET
    * Mandatory Params: event_id & date in (yyyy-mm-dd) format
    **/
    function mfs_get_user_timeslots( WP_REST_Request $request ) {
      $parameters = $request->get_params();

      if( !isset( $parameters["event_id"] ) || $parameters["event_id"] == "" ) {
        return $this->mfs_error_response( 'missing_parameters', 'Event ID is mandatory !', 400 );
      } else if( !isset( $parameters["date"] ) || $parameters["date"] == "" ) {
        return $this->mfs_error_response( 'missing_parameters', 'Date is mandatory !', 400 );
      } else if( !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $parameters["date"] ) ) {
        return $this->mfs_error_response( 'invalid_parameters', 'Invalid Date !', 400 );
      } else{
        $eventData = $this->dbObject->mfs_get_event_data( $parameters['event_id'] );
        if( !empty( $eventData ) ){
          $message = "Available time slots fetched successfully";
          $eventStructure = json_decode($eventData->form_structure);

          // setting for disabled dates
          $disableDays = array();
          foreach( $eventStructure[0][0]->working_dates as $key => $value ){
            if( !$value || $value === false ){
              $disableDays[] = $key;
            }
          }

          // setting for avaiable slots
          $duration = $eventStructure[0][0]->services[0]->duration;
          $startTimeHour = $eventStructure[0][0]->allOH[0]->openhours[0]->h1;
          $startTimeMinute = $eventStructure[0][0]->allOH[0]->openhours[0]->m1;
          $endTimeHour = $eventStructure[0][0]->allOH[0]->openhours[0]->h2;
          $sendTimeMinute = $eventStructure[0][0]->allOH[0]->openhours[0]->m1;
          $timeFormat = $eventStructure[0][0]->militaryTime;

          // fetch pre booked events
          $bookedSlots = array();
          $bookedSlotsData = $this->dbObject->mfs_get_event_booked_slot( $parameters["event_id"] );
          foreach( $bookedSlotsData as $item ) {
            $data = unserialize( $item->posted_data );
            foreach( $data["apps"] as $app ) {
              $scheduledDate = date( "Y-m-d", strtotime( $app["date"] ) );
              if ( $app["cancelled"] != 'Cancelled' && $app["cancelled"] != 'Cancelled by customer' && $parameters["date"] == $scheduledDate ) {
                $slotStr = explode( '/', $app["slot"] );
                if( $timeFormat !== $app['military'] ){
                  $timeArray = explode( ':', $slotStr[0] );
                  $minutesArray = explode( ' ', $timeArray[1] );

                  if( $timeFormat ) {
                    $bookedSlot = $this->mfs_format_time_slot( $timeArray[0], $minutesArray[0] );
                  } else{
                    if( $minutesArray[1] == 'PM' ) {
                      $bookedSlot = ( $timeArray[0] + 12 ) . ":" . $minutesArray[0];
                    } else{
                      $bookedSlot = $timeArray[0] . ":" . $minutesArray[0];

                    }
                  }
                } else{
                  $bookedSlot = $slotStr[0];
                }

                $bookedSlots[] = $bookedSlot;
              }
            }
          }

          if( $startTimeHour < current_time( 'G' ) ) {
            $startTimeHour = current_time( 'G' );
          }

          $avlTimeSlots = array();
          for( $hours = $startTimeHour; $hours < $endTimeHour; $hours++ ) {
            for( $minutes = 0; $minutes <= ( 60 - $duration ); $minutes = $minutes + $duration ) {
              if( strlen( $minutes ) <= 1 ) {
                $minutes = "0" . $minutes;
              }

              if( $timeFormat ) {
                $slot = $this->mfs_format_time_slot( $hours, $minutes );
              } else {
                $slot = $hours . ":$minutes";
              }

              if( !in_array( $slot, $bookedSlots ) ) {
                $avlTimeSlots[] = $slot;
              }
            }
          }

          $result = array(
            "date_format"     => $eventData->date_format,
            "successMessage"  => $eventData->fp_message,
            "duration"        => $duration,
            "disableDays"     => $disableDays,
            "avlTimeSlots"    => $avlTimeSlots
          );
        } else{
          $message = "No slot is available !";
        }

        $data = array(
          'code' => 'success',
          'message' => $message,
          'data' => $result
        );

        return $this->mfs_success_response( $data, 200 );
      }
    }

    /**
    * Name: mfs_book_event_slot
    * Author: Shubham
    * Description: Used to book event slot
    * API Route: /apis/v1/mfs_book_slot
    * Method: POST
    * Mandatory Params: event_id, date (dd/mm/yyyy), slot_time, duration, email
    * Optional Params: name & reason
    **/
    function mfs_book_event_slot( WP_REST_Request $request ) {
      $parameters = $request->get_params();
      
      // $this->dbObject->mfs_book_slot();

      $result = array();
      $data = array(
        'code' => 'success',
        'message' => "Event slot booked successfully",
        'data' => $result
      );

      return $this->mfs_success_response( $data, 200 );
    }
  }
}
